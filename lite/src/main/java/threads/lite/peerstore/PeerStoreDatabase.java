package threads.lite.peerstore;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;

@androidx.room.Database(entities = {Peer.class}, version = 3, exportSchema = false)
@TypeConverters({ID.class, Multiaddr.class})
public abstract class PeerStoreDatabase extends RoomDatabase {

    public abstract PeerStoreDao peerStoreDao();

}
