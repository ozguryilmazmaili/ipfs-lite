package threads.lite.core;

import androidx.annotation.NonNull;

import net.luminis.quic.server.ServerConnector;

import java.net.DatagramSocket;
import java.util.function.Consumer;

import threads.lite.server.ServerSession;

public interface Server {
    void shutdown();

    int getPort();

    int numServerConnections();

    @NonNull
    ServerSession getServerSession();

    @NonNull
    Consumer<Connection> getConnectConsumer();

    @NonNull
    Consumer<Connection> getClosedConsumer();

    @NonNull
    ServerConnector getServerConnector();

    @NonNull
    DatagramSocket getSocket();
}
