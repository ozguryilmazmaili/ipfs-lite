package threads.lite.core;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.LogUtils;
import threads.lite.utils.DataHandler;

public interface Transport {
    String TAG = Transport.class.getSimpleName();

    static FrameReader getFrameReader(Transport transport) {
        return data -> {
            switch (transport.getType()) {
                case MUXED:
                case SECURED:
                case HANDSHAKE:
                    return bigEndianUnsignedShortReader(data);
                case PLAIN:
                    return unsignedVarintReader(data);
                default:
                    throw new IllegalStateException("not supported transport reader");
            }
        };
    }

    private static ByteBuffer bigEndianUnsignedShortReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        int length = DataHandler.getBigEndianUnsignedShort(data);
        LogUtils.debug(TAG, "bigEndianUnsignedShortReader Length " + length);
        return ByteBuffer.allocate(length);
    }

    private static ByteBuffer unsignedVarintReader(ByteBuffer data) throws IOException {
        Objects.requireNonNull(data);
        int length = DataHandler.readUnsignedVariant(data);
        LogUtils.debug(TAG, "unsignedVarintReader Length " + length);
        return ByteBuffer.allocate(length);
    }

    Type getType();

    Stream getStream();

    enum Type {PLAIN, HANDSHAKE, SECURED, MUXED}
}
