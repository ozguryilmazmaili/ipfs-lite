package threads.lite.core;

import androidx.annotation.NonNull;


public interface Validator {


    @NonNull
    IpnsEntity validate(byte[] key, byte[] value) throws Exception;

    // return 1 for rec and -1 for cmp and 0 for both equal
    int compare(@NonNull IpnsEntity rec, @NonNull IpnsEntity cmp);

}
