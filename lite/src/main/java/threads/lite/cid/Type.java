package threads.lite.cid;

public class Type {
    public static final String IP4 = "ip4";
    public static final String IP6 = "ip6";
    public static final String UDP = "udp";
    public static final String P2P = "p2p";
    public static final String P2PCIRCUIT = "p2p-circuit";
    public static final String DNSADDR = "dnsaddr";
    public static final String DNS = "dns";
    public static final String DNS4 = "dns4";
    public static final String DNS6 = "dns6";
    public static final String QUIC = "quic";
    public static final String TCP = "tcp";
    public static final String QUICV1 = "quic-v1";
}
