package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity
public final class Peer {

    @PrimaryKey
    @ColumnInfo(name = "key")
    private final short key;

    @NonNull
    @ColumnInfo(name = "multiaddr")
    @TypeConverters(Multiaddr.class)
    private final Multiaddr multiaddr;

    @NonNull
    @ColumnInfo(name = "id")
    @TypeConverters(ID.class)
    private final ID id;

    @Ignore
    private int rtt = Integer.MAX_VALUE;  // temporary value
    @Ignore
    private boolean replaceable = true; // temporary value

    // Note: fore creating a peer instance use the static create method
    public Peer(short key, @NonNull Multiaddr multiaddr, @NonNull ID id) {
        this.key = key;
        this.multiaddr = multiaddr;
        this.id = id;
    }

    @NonNull
    public static Peer create(@NonNull Multiaddr multiaddr) throws Exception {
        ID id = ID.convertPeerID(multiaddr.getPeerId());
        return new Peer(createKey(id), multiaddr, id);
    }

    public static short createKey(@NonNull ID id) {
        short shortKey = (short) id.hashCode();
        return shortKey > 0 ? shortKey : (short) -shortKey;
    }

    public int getRtt() {
        return rtt;
    }

    public void setRtt(int rtt) {
        this.rtt = rtt;
    }

    public short getKey() {
        return key;
    }

    @NonNull
    public ID getId() {
        return id;
    }

    @NonNull
    public ID getID() {
        return id;
    }

    @NonNull
    public PeerId getPeerId() {
        return multiaddr.getPeerId();
    }

    @NonNull
    public Multiaddr getMultiaddr() {
        return multiaddr;
    }

    @NonNull
    @Override
    public String toString() {
        return "Peer{" +
                "multiaddr=" + multiaddr +
                ", id=" + id +
                ", replaceable=" + replaceable +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peer peer = (Peer) o;
        return multiaddr.equals(peer.multiaddr);
    }

    @Override
    public int hashCode() {
        return multiaddr.hashCode();
    }

    public boolean isReplaceable() {
        return replaceable;
    }

    public void setReplaceable(boolean replaceable) {
        this.replaceable = replaceable;
    }

}
