package threads.lite.utils;


public interface Splitter {

    int nextBytes(byte[] bytes) throws Exception;

    boolean done();
}
