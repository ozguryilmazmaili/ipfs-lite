package threads.lite.dag;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.List;

import merkledag.pb.Merkledag;
import threads.lite.core.Link;
import unixfs.pb.Unixfs;


public final class DagDirectory {


    public static DirectoryNode createDirectory(@NonNull List<Link> links) {
        Unixfs.Data.Builder builder = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory);
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        long fileSize = 0;
        for (Link link : links) {
            long size = link.getSize();
            fileSize = fileSize + size;
            builder.addBlocksizes(size);

            Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                    .setName(link.getName())
                    .setTsize(link.getSize());

            lnb.setHash(ByteString.copyFrom(link.getCid().bytes()));

            pbn.addLinks(lnb.build());
        }
        builder.setFilesize(fileSize);
        byte[] unixData = builder.build().toByteArray();

        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), fileSize);

    }

    public static DirectoryNode createDirectory() {
        byte[] unixData = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory)
                .build().toByteArray();
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), 0);
    }


    public static boolean isDirectory(@NonNull Merkledag.PBNode node) throws Exception {
        Unixfs.Data unixData = DagReader.getData(node);

        return unixData.getType() == unixfs.pb.Unixfs.Data.DataType.Directory;
    }

    public static class DirectoryNode {
        private final Merkledag.PBNode node;
        private final long size;

        public DirectoryNode(Merkledag.PBNode node, long size) {
            this.node = node;
            this.size = size;
        }

        public Merkledag.PBNode getNode() {
            return node;
        }

        public long getSize() {
            return size;
        }
    }

}
