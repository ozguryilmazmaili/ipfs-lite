package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.server.ServerConnector;

import java.net.DatagramSocket;
import java.util.function.Consumer;

import threads.lite.core.Connection;
import threads.lite.core.Server;
import threads.lite.server.ServerSession;

public class LiteServer implements Server {
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector serverConnector;
    @NonNull
    private final ServerSession serverSession;
    @NonNull
    private final Consumer<Connection> connectConsumer;
    @NonNull
    private final Consumer<Connection> closedConsumer;


    public LiteServer(@NonNull DatagramSocket socket,
                      @NonNull ServerConnector serverConnector,
                      @NonNull ServerSession serverSession,
                      @NonNull Consumer<Connection> connectConsumer,
                      @NonNull Consumer<Connection> closedConsumer) {
        this.socket = socket;
        this.serverConnector = serverConnector;
        this.serverSession = serverSession;
        this.connectConsumer = connectConsumer;
        this.closedConsumer = closedConsumer;
    }

    @NonNull
    @Override
    public Consumer<Connection> getClosedConsumer() {
        return closedConsumer;
    }

    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }

    @NonNull
    public ServerConnector getServerConnector() {
        return serverConnector;
    }

    @NonNull
    @Override
    public ServerSession getServerSession() {
        return serverSession;
    }

    @NonNull
    @Override
    public Consumer<Connection> getConnectConsumer() {
        return connectConsumer;
    }

    @Override
    public void shutdown() {
        serverConnector.shutdown();
    }

    @Override
    public int getPort() {
        return socket.getLocalPort();
    }

    @Override
    public int numServerConnections() {
        return serverConnector.numConnections();
    }
}
