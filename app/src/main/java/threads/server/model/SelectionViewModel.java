package threads.server.model;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SelectionViewModel extends ViewModel {
    @NonNull
    private final MutableLiveData<Boolean> showFab = new MutableLiveData<>(true);

    @NonNull
    private final MutableLiveData<Uri> uri = new MutableLiveData<>(null);

    @NonNull
    public MutableLiveData<Uri> getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        getUri().postValue(uri);
    }

    @NonNull
    public MutableLiveData<Boolean> getShowFab() {
        return showFab;
    }

    public void setShowFab(boolean show) {
        getShowFab().postValue(show);
    }

}
