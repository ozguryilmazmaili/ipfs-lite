package threads.server.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.Objects;

import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.Content;

public class ContentDialogFragment extends DialogFragment {

    public static final String TAG = ContentDialogFragment.class.getSimpleName();


    public static ContentDialogFragment newInstance(@NonNull String message, @NonNull String url) {
        Bundle bundle = new Bundle();
        bundle.putString(Content.TEXT, message);
        bundle.putString(Content.URL, url);
        ContentDialogFragment fragment = new ContentDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.content_info, null);

        ImageView imageView = view.findViewById(R.id.uri_qrcode);
        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);
        String title = getString(R.string.information);
        String message = bundle.getString(Content.TEXT, "");
        String url = bundle.getString(Content.URL, "");


        TextView page = view.findViewById(R.id.page);
        page.setCompoundDrawablePadding(8);
        if (url.isEmpty()) {
            page.setVisibility(View.GONE);
        } else {
            page.setText(url);
        }

        try {
            Bitmap bitmap = getBitmap(url);
            Glide.with(requireContext()).load(bitmap).into(imageView);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());

        builder.setTitle(title)
                .setMessage(message)
                .setView(view)
                .create();

        return builder.create();
    }

    private static final int QR_CODE_SIZE = 250;

    private static Bitmap getBitmap(@NonNull String content) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(content,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
