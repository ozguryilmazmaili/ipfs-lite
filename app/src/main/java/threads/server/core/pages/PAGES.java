package threads.server.core.pages;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import threads.lite.cid.Cid;


public class PAGES {

    private static volatile PAGES INSTANCE = null;

    private final PageDatabase pageDatabase;


    private PAGES(PageDatabase pageDatabase) {
        this.pageDatabase = pageDatabase;
    }

    @NonNull
    private static PAGES createPages(@NonNull PageDatabase pageDatabase) {
        return new PAGES(pageDatabase);
    }

    public static PAGES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PAGES.class) {
                if (INSTANCE == null) {
                    PageDatabase pageDatabase = Room.databaseBuilder(context,
                                    PageDatabase.class,
                                    PageDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().build();

                    INSTANCE = PAGES.createPages(pageDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public Page createPage(@NonNull String id) {
        return new Page(id);
    }


    public void storePage(@NonNull Page page) {
        pageDatabase.pageDao().insertPage(page);
    }

    @Nullable
    public Page getPage(@NonNull String id) {
        return pageDatabase.pageDao().getPage(id);
    }

    @NonNull
    public PageDatabase getPageDatabase() {
        return pageDatabase;
    }


    public void setPageContent(@NonNull String id, @NonNull Cid cid) {
        pageDatabase.pageDao().setContent(id, cid);
    }

    public void setPageSequence(@NonNull String id, long sequence) {
        pageDatabase.pageDao().setSequence(id, sequence);
    }


    public void clear() {
        getPageDatabase().clearAllTables();
    }

    @Nullable
    public Cid getPageContent(String id) {
        return getPageDatabase().pageDao().getContent(id);
    }

    public void incrementPageSequence(String id) {
        pageDatabase.pageDao().incrementSequence(id);
    }


}
