package threads.server.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkManager;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.AutonatResult;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Server;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.DOCS;
import threads.server.core.pages.Page;
import threads.server.work.ReservationWorker;

public class DaemonService extends Service {

    private static final String ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE";
    private static final String ACTION_START_SERVICE = "ACTION_START_SERVICE";
    private static final AtomicBoolean isRunning = new AtomicBoolean(false);

    private static final String TAG = DaemonService.class.getSimpleName();
    public static Reachability REACHABILITY = Reachability.UNKNOWN;
    private final AtomicBoolean runAutonat = new AtomicBoolean(false);
    private ConnectivityManager.NetworkCallback networkCallback;
    private NsdManager nsdManager;
    private DiscoveryService discoveryService;
    private Server server;

    public static void start(@NonNull Context context) {
        try {
            if (!isRunning.getAndSet(true)) {
                Intent intent = new Intent(context, DaemonService.class);
                intent.setAction(ACTION_START_SERVICE);
                ContextCompat.startForegroundService(context, intent);
            }
        } catch (Throwable throwable) {
            isRunning.set(false);
            LogUtils.error(TAG, throwable);
        }
    }

    private void unRegisterNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            connectivityManager.unregisterNetworkCallback(networkCallback);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void autonat() {
        try {
            if (!runAutonat.getAndSet(true)) {
                REACHABILITY = Reachability.UNKNOWN;
                buildNotification();

                IPFS ipfs = IPFS.getInstance(getApplicationContext());
                AutonatResult result = ipfs.autonat(server);

                if (!result.success()) {

                    if (result.getNatType() == AutonatResult.NatType.SYMMETRIC) {
                        DaemonService.REACHABILITY = DaemonService.Reachability.LOCAL;
                    } else {
                        DaemonService.REACHABILITY = DaemonService.Reachability.RELAYS;

                        ReservationWorker.reservations(getApplicationContext(),
                                ExistingWorkPolicy.REPLACE, 0);
                    }
                } else {
                    DaemonService.REACHABILITY = DaemonService.Reachability.GLOBAL;
                }

                buildNotification();
                runAutonat.set(false);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            networkCallback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    autonat();
                }

                @Override
                public void onLost(Network network) {
                    try {
                        REACHABILITY = Reachability.NONE;
                        buildNotification();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            };


            connectivityManager.registerDefaultNetworkCallback(networkCallback);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction().equals(ACTION_START_SERVICE)) {
            buildNotification();
        } else if (intent.getAction().equals(ACTION_STOP_SERVICE)) {
            try {
                stopForeground(STOP_FOREGROUND_REMOVE);
            } finally {
                stopSelf();
            }
        }

        return START_NOT_STICKY;
    }

    private void buildNotification() {
        try {
            Objects.requireNonNull(server);
            int port = server.getPort();
            Notification.Builder builder = new Notification.Builder(
                    getApplicationContext(), InitApplication.DAEMON_CHANNEL_ID);

            Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
            int viewID = (int) System.currentTimeMillis();
            PendingIntent viewIntent = PendingIntent.getActivity(getApplicationContext(),
                    viewID, notifyIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);


            Intent stopIntent = new Intent(getApplicationContext(), DaemonService.class);
            stopIntent.setAction(ACTION_STOP_SERVICE);
            int requestID = (int) System.currentTimeMillis();
            PendingIntent stopPendingIntent = PendingIntent.getService(
                    getApplicationContext(), requestID, stopIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause),
                    getApplicationContext().getString(R.string.shutdown),
                    stopPendingIntent).build();

            builder.setSmallIcon(R.drawable.access_point_network);
            int connections = server.numServerConnections();
            if (connections > 0) {
                builder.setColor(Color.parseColor("#006972"));
            }
            builder.setSubText(getApplicationContext().getString(
                    R.string.server_connections) + " " + connections);
            builder.addAction(action);

            String text = "";
            if (REACHABILITY == Reachability.UNKNOWN) {
                text = getString(R.string.service_reachable_unknown);
            } else if (REACHABILITY == Reachability.LOCAL) {
                text = getString(R.string.service_local_reachable);
            } else if (REACHABILITY == Reachability.NONE) {
                text = getString(R.string.service_not_reachable);
            } else if (REACHABILITY == Reachability.GLOBAL) {
                text = getString(R.string.service_reachable);
            } else if (REACHABILITY == Reachability.RELAYS) {
                text = getString(R.string.service_relays_reachable);
            }

            builder.setContentTitle(getString(R.string.service_is_running, String.valueOf(port)));
            builder.setStyle(new Notification.BigTextStyle().bigText(text));
            builder.setContentIntent(viewIntent);
            builder.setCategory(Notification.CATEGORY_SERVICE);
            builder.setOnlyAlertOnce(true);

            Notification notification = builder.build();
            startForeground(TAG.hashCode(), notification);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void registerService() {
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            PeerId peerId = ipfs.self();
            String ownServiceName = peerId.toBase58();


            NsdServiceInfo serviceInfo = new NsdServiceInfo();
            try {
                Multiaddr address = docs.getSiteLocalAddress(server);
                Objects.requireNonNull(address);
                serviceInfo.setAttribute(Protocol.DNSADDR.getType(), address.toString());
                LogUtils.error(TAG, Protocol.DNSADDR.getType() + "=" + address);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
            serviceInfo.setServiceName(ownServiceName);
            serviceInfo.setServiceType(Settings.SERVICE);
            serviceInfo.setPort(server.getPort());
            nsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
            Objects.requireNonNull(nsdManager);
            nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD,
                    RegistrationService.getInstance());

            discoveryService = new DiscoveryService(getApplicationContext(), nsdManager);
            nsdManager.discoverServices(
                    Settings.SERVICE, NsdManager.PROTOCOL_DNS_SD, discoveryService);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void unRegisterService() {
        try {
            if (nsdManager != null) {
                nsdManager.unregisterService(RegistrationService.getInstance());
                if (discoveryService != null) {
                    nsdManager.stopServiceDiscovery(discoveryService);
                    discoveryService = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unRegisterNetworkCallback();
            unRegisterService();
            isRunning.set(false);
            server.shutdown();
            // closing app
            WorkManager.getInstance(getApplicationContext()).cancelAllWork();

            removeFromResents();

            System.exit(0);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void removeFromResents() {
        try {
            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            if (am != null) {
                List<ActivityManager.AppTask> tasks = am.getAppTasks();
                if (tasks != null && tasks.size() > 0) {
                    tasks.get(0).setExcludeFromRecents(true);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            ExecutorService service = Executors.newSingleThreadExecutor();
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());

            server = ipfs.startServer(5001, connection -> {
                try {
                    buildNotification();
                    if (Multiaddr.isLocalAddress(
                            connection.getRemoteAddress().getAddress())) {

                        service.execute(() -> {

                            Page page = docs.getHomePage();
                            if (page != null) {
                                try {
                                    Cid cid = page.getCid();
                                    Objects.requireNonNull(cid);

                                    IpnsRecord ipnsRecord =
                                            ipfs.createSelfSignedIpnsRecord(page.getSequence(),
                                                    ipfs.encodeIpnsData(cid));

                                    ipfs.push(connection, ipnsRecord);
                                } catch (Throwable throwable) {
                                    LogUtils.error(TAG, throwable);
                                }
                            }
                        });
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }, connection -> buildNotification(), peerId -> false);

            registerNetworkCallback();
            registerService();
            autonat();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public enum Reachability {
        UNKNOWN, LOCAL, NONE, RELAYS, GLOBAL
    }
}
