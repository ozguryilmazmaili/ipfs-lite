package threads.server.utils;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import threads.server.core.files.Proxy;

@SuppressWarnings("WeakerAccess")
public class FileDiffCallback extends DiffUtil.Callback {
    private final List<Proxy> mOldList;
    private final List<Proxy> mNewList;

    public FileDiffCallback(List<Proxy> messages, List<Proxy> proxies) {
        this.mOldList = messages;
        this.mNewList = proxies;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areItemsTheSame(mNewList.get(
                newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areContentsTheSame(mNewList.get(newItemPosition));
    }
}
