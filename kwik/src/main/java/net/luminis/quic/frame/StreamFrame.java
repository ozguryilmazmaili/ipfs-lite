/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.frame;

import androidx.annotation.NonNull;

import net.luminis.quic.InvalidIntegerEncodingException;
import net.luminis.quic.VariableLengthInteger;
import net.luminis.quic.packet.QuicPacket;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;


public final class StreamFrame extends QuicFrame implements Comparable<StreamFrame> {
    private final StreamType streamType;
    private final int streamId;
    private final long offset;
    private final int length;
    private final byte[] streamData;
    private final boolean isFinal;
    private final int frameLength;

    public StreamFrame(StreamType streamType, int streamId, boolean isFinal, long offset,
                       int length, byte[] streamData, int frameLength) {
        this.streamType = streamType;
        this.streamId = streamId;
        this.isFinal = isFinal;
        this.offset = offset;
        this.length = length;
        this.streamData = streamData;
        this.frameLength = frameLength;
    }

    public StreamFrame(int streamId, long streamOffset, byte[] applicationData, boolean fin) {
        this.streamType = StreamType.get(streamId & 0x03);
        this.streamId = streamId;
        this.offset = streamOffset;
        this.streamData = applicationData;
        this.length = applicationData.length;
        this.isFinal = fin;
        this.frameLength = frameLength(streamId, offset, length);
    }

    public static int frameLength(int streamId, long offset, int length) {
        return 1  // frame type
                + VariableLengthInteger.bytesNeeded(streamId)
                + VariableLengthInteger.bytesNeeded(offset)
                + VariableLengthInteger.bytesNeeded(length)
                + length;
    }

    public static StreamFrame parse(ByteBuffer buffer) throws InvalidIntegerEncodingException {
        int startPosition = buffer.position();

        int frameType = buffer.get();
        boolean withOffset = ((frameType & 0x04) == 0x04);
        boolean withLength = ((frameType & 0x02) == 0x02);
        boolean isFinal = ((frameType & 0x01) == 0x01);

        int streamId = VariableLengthInteger.parse(buffer);
        StreamType streamType = StreamType.get(streamId & 0x03);

        long offset = 0;
        if (withOffset) {
            offset = VariableLengthInteger.parseLong(buffer);
        }
        int length;
        if (withLength) {
            length = VariableLengthInteger.parse(buffer);
        } else {
            length = buffer.limit() - buffer.position();
        }

        byte[] streamData = new byte[length];
        buffer.get(streamData);
        int frameLength = buffer.position() - startPosition;

        return new StreamFrame(streamType, streamId, isFinal, offset, length,
                streamData, frameLength);
    }

    @Override
    public void serialize(ByteBuffer buffer) {
        if (frameLength > buffer.remaining()) {
            throw new IllegalArgumentException();
        }

        byte baseType = (byte) 0x08;
        byte frameType = (byte) (baseType | 0x04 | 0x02);  // OFF-bit, LEN-bit, (no) FIN-bit
        if (isFinal) {
            frameType |= 0x01;
        }
        buffer.put(frameType);
        VariableLengthInteger.encode(streamId, buffer);
        VariableLengthInteger.encode(offset, buffer);
        VariableLengthInteger.encode(length, buffer);
        buffer.put(streamData);
    }

    @Override
    public int getFrameLength() {
        return frameLength;
    }

    @NonNull
    @Override
    public String toString() {
        return "StreamFrame[" + streamId + "(" + streamType.abbrev + ")" + ","
                + offset + "," + length + (isFinal ? ",f" : "") + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StreamFrame)) return false;
        StreamFrame that = (StreamFrame) o;
        return streamId == that.streamId &&
                offset == that.offset &&
                length == that.length &&
                isFinal == that.isFinal &&
                Arrays.equals(streamData, that.streamData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(streamId, offset, length);
    }

    @Override
    public int compareTo(StreamFrame other) {
        if (this.offset != other.getOffset()) {
            return Long.compare(this.offset, other.getOffset());
        } else {
            return Long.compare(this.length, other.getLength());
        }
    }

    public int getStreamId() {
        return streamId;
    }

    public long getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }

    public ByteBuffer getStreamData() {
        return ByteBuffer.wrap(streamData);
    }

    public long getUpToOffset() {
        return offset + length;
    }

    public boolean isFinal() {
        return isFinal;
    }

    @Override
    public void accept(FrameProcessor3 frameProcessor, QuicPacket packet, Instant timeReceived) {
        frameProcessor.process(this, packet, timeReceived);
    }
}
